%iterate s={0.0625,0.25,1,4,16}
epsilon=16;
C=100;
for s = [0.0625, 0.25, 1, 4, 16]
    [x,y,alphap,alphan,b] = trainSVM('motorcycle_X.txt', 'motorcycle_y.txt',C,s,epsilon);
    plotSVM(x,y,alphap,alphan,b,s,epsilon);
end

%iterate epsilon={1,4,16,64,256}
s=1;
for epsilon = [1, 4, 16, 64, 256]
    [x,y,alphap,alphan,b] = trainSVM('motorcycle_X.txt', 'motorcycle_y.txt',C,s,epsilon);
    plotSVM(x,y,alphap,alphan,b,s,epsilon);
end