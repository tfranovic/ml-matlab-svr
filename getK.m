function [ K ] = getK(x, s)
%GETK Computation of kernel matrix
%   Computation of kernel matrix using the radial kernel function
n=size(x,1);
K=zeros(n,n);
for i=1:n
    for j=1:n
        K(i,j)=k(x(i,:),x(j,:),s);
    end
end
end