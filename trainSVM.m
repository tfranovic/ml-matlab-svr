function [x, y, alphap, alphan, b] = trainSVM(x, y, C, s, epsilon)
%TRAINSVM Training SVM for regression
%x=load(x);
%y=load(y);
N=size(x,1);
%normalize data
mu = mean(x);
sigma = std(x);
x = (x - repmat(mu, N, 1)) ./ repmat(sigma, N, 1);

%get K matrix
K=getK(x,s);

%get alphas
[alphap, alphan]=getAlphas(K,C,y,epsilon);

%get b
b=getB(x,y,alphap,alphan,epsilon,s,C);
end

