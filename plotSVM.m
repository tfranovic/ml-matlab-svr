function [ ] = plotSVM(x,y,alphap,alphan,b,s,epsilon)
x2=linspace(min(x),max(x),2000)';
y2=zeros(2000,1);
for i=1:2000
    y2(i)=decide(x,x2(i),alphap,alphan,b,s);
end
figure();
hold on;
scatter(x,y,'+b');
scatter(x(alphap>0),y(alphap>0),'Ok');
scatter(x(alphan>0),y(alphan>0),'Ok');
plot(x2,y2,'Color', [0.5 1 0.5],'LineWidth', 2);
title(strcat('s=', num2str(s), '  \epsilon=', num2str(epsilon)));
end

