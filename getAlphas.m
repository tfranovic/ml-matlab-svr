function [ alphap, alphan ] = getAlphas( K, C, y, epsilon)
%GETALPHAS Calculation of support vector coefficients
%   Function that computes the support vector coefficients from the kernel matrix
N=size(K,1);
H = [+K, -K; -K, +K];
f = [epsilon * ones(N, 1) - y; epsilon * ones(N, 1) + y];
A = [];
b = [];
Aeq = [+ones(1, N), -ones(1, N)];
beq = 0;
lb = zeros(2 * N, 1);
ub = C * ones(2 * N, 1);
options = optimset('Algorithm', 'interior-point-convex', 'Display', 'off');
alphas=quadprog(H,f,A,b,Aeq,beq,lb,ub,[],options);
alphap=alphas(1:N,:);
alphan=alphas(N+1:2*N,:);
alphap(alphap < C * 0.001) = 0;
alphap(alphap > C * 0.999) = C;
alphan(alphan < C * 0.001) = 0;
alphan(alphan > C * 0.999) = C;
end

