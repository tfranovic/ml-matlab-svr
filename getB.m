function [ b ] = getB( x, y, alphap, alphan, epsilon, s, C)
%GETB Calculation borderline distance
%   Calculation if the distance of the hyperplane from the origin
b=0;
N=length(y);
for i=1:N
    sums=0;
    for j=1:N
        sums=sums+(alphap(j)-alphan(j))*k(x(j,:),x(i,:),s);
    end
    b=b+((alphap(i)>0)*(alphap(i)<C)).*(y(i)-sums-epsilon);
end
b=b/sum((alphap>0).*(alphap<C));
if (isnan(b))
    b=0;
end

