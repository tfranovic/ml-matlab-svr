function [ k ] = k(x,y,s)
%K Gaussian kernel function
k=x-y;
k=-sum(k.^2)/(s^2);
k=exp(k);
end